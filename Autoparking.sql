CREATE TABLE Autoparking(
	car_id 			AutoIncrement 	NOT NULL,
	car_number 		Text(50) 		NOT NULL,
	entry_date 		DATETIME 		NOT NULL,
	departure_date 	DATETIME
)