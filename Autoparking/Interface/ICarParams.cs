﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoparking
{
    public interface ICarParams
    {
        /// <summary>
        /// id машины
        /// </summary>
        int Id { get; set; }
        /// <summary>
        /// Номер машины
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// дата въезда
        /// </summary>
        DateTime EntryDate { get; set; }
        /// <summary>
        /// дата выезда
        /// </summary>
        DateTime DepartureDate { get; set; }
    }
}
