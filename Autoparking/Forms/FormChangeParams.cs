﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoparking
{
    public partial class FormChangeParams : Form
    {
        mdb mdb;
        int id;
        public FormChangeParams(CarParams carParams, mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            id = carParams.Id;
            tbNumber.Text = carParams.Number;
            dtpEntryDate.Value = carParams.EntryDate;
            dtpEntryTime.Value = carParams.EntryDate;
            //Если машина ещё на парковке
            if (carParams.DepartureDate.ToString("G") != new DateTime().ToString("G"))
            {
                //Скрыть кнопку "изменить дату выезда"
                btnChangeDepartureDate.Visible = false;
                //отобразить кнопку удаления даты выезда
                btnDeleteDepartureDate.Visible = true;
                //Выставление параметров даты и времени выезда
                dtpDepartureDate.Visible = true;
                dtpDepartureTime.Visible = true;
                dtpDepartureDate.Value = carParams.DepartureDate;
                dtpDepartureTime.Value = carParams.DepartureDate;
            }
                
        }
        //Сохранить изменения
        private void btnSave_Click(object sender, EventArgs e)
        {
            CarParams car = new CarParams();
            car.Number = tbNumber.Text;
            car.EntryDate = DateTime.Parse(String.Format("{0} {1}", dtpEntryDate.Value.ToShortDateString(), dtpEntryTime.Value.ToShortTimeString()));
            if (!btnChangeDepartureDate.Visible)
                car.DepartureDate = DateTime.Parse(String.Format("{0} {1}", dtpDepartureDate.Value.ToShortDateString(), dtpDepartureTime.Value.ToShortTimeString()));
            else
                car.DepartureDate = new DateTime();

            mdb.DoChangeDataById(id, car);
            this.Close();
        }

        private void btnChangeDepartureDate_Click(object sender, EventArgs e)
        {
            btnChangeDepartureDate.Visible = false;
            btnDeleteDepartureDate.Visible = true;
            dtpDepartureDate.Visible = true;
            dtpDepartureTime.Visible = true;
        }

        private void btnDeleteDepartureDate_Click(object sender, EventArgs e)
        {
            btnChangeDepartureDate.Visible = true;
            btnDeleteDepartureDate.Visible = false;
        }
    }
}
