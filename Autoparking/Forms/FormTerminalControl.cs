﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoparking
{
    public partial class FormTerminalControl : Form
    {
        mdb mdb;
        List<CarParams> carList = new List<CarParams>();
        public FormTerminalControl(mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            //Получить список всех автомобилей, чтобы в дальнейшем не тыкаться каждый раз в бд
            carList = mdb.GetAllCars();
            DoSetAllCarList();
        }

        public void DoSetAllCarList()
        {
            //Удалить все строки из таблицы
            dgvCarsInformation.Rows.Clear();
            int index = 0;
            foreach(CarParams car in carList)
            {
                //Добавить всех клиентов на таблицу
                dgvCarsInformation.Rows.Add();
                dgvCarsInformation.Rows[index].Cells["id"].Value = car.Id;
                dgvCarsInformation.Rows[index].Cells["Number"].Value = car.Number;
                dgvCarsInformation.Rows[index].Cells["EntryDate"].Value = car.EntryDate;
                if(car.DepartureDate.ToString("G") != new DateTime().ToString("G"))
                {
                    dgvCarsInformation.Rows[index].Cells["DepartureDate"].Value = car.DepartureDate;
                }
                index++;
            }
        }
        /// <summary>
        /// Проверить дату выезда (нужен для сортировки)
        /// </summary>
        /// <param name="date"></param>
        /// <param name="dateNow"></param>
        /// <returns></returns>
        public bool DoSortList(DateTime date, DateTime dateNow)
        {
            //Если разница между днями больше чем 1, то не добавлять клиента на таблицу
            if((dateNow.Day - date.Day) >= 2)
            {
                return false;
            }
            else
            {
                //Если разница между текуюим временем и временем заезда больше 1, значит клиент больше 24 часов на парковке
                int result = dateNow.Hour - date.Hour;
                if(result > 1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// Отобразить клиентов за 24 часа
        /// </summary>
        public void DoSetCarsByDay()
        {
            dgvCarsInformation.Rows.Clear();
            DateTime dateNow = DateTime.Now;
            //Проверяем клиентов за последние 24 часа по дате въезда
            List<CarParams> dayCarList = carList.Where(t=> DoSortList(t.EntryDate, dateNow)).ToList();
            int index = 0;
            foreach (CarParams car in dayCarList)
            {
                dgvCarsInformation.Rows.Add();
                dgvCarsInformation.Rows[index].Cells["id"].Value = car.Id;
                dgvCarsInformation.Rows[index].Cells["Number"].Value = car.Number;
                dgvCarsInformation.Rows[index].Cells["EntryDate"].Value = car.EntryDate;
                //Если дата выезда не задана, то оставить ячейку пустой
                if (car.DepartureDate.ToString("G") != new DateTime().ToString("G"))
                {
                    dgvCarsInformation.Rows[index].Cells["DepartureDate"].Value = car.DepartureDate;
                }
                index++;
            }
        }

        private void btnSetDayClient_Click(object sender, EventArgs e)
        {
            DoSetCarsByDay();
        }

        private void btnSetAllClient_Click(object sender, EventArgs e)
        {
            DoSetAllCarList();
        }

        private void dgvCarsInformation_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Отобразить контекстное меню по нажатию на правую кнопку мыши
            if((e.RowIndex > -1) && (e.Button == MouseButtons.Right))
            {
                dgvCarsInformation.ClearSelection();
                //Выделить строку
                dgvCarsInformation.Rows[e.RowIndex].Selected = true;
                dgvCarsInformation.Focus();
                //Отобразить контекстное меню
                cmsCarProps.Show(Cursor.Position);
            }
        }

        private void cmsDeparture_Click(object sender, EventArgs e)
        {
            //Получить индекс выделенной строки
            int index = dgvCarsInformation.SelectedRows[0].Index;
            //Если дата выезда не задана, то зарегистрировать выезд
            if(dgvCarsInformation.Rows[index].Cells[3].Value == null)
            {
                DateTime now = DateTime.Now;
                mdb.DoUpdateDepartureDateByNumber(dgvCarsInformation.Rows[index].Cells[1].Value.ToString(), now);
            }

            carList = mdb.GetAllCars();
            DoSetAllCarList();
        }
        /// <summary>
        /// Открыть форму для редактирования данных из контекстного меню
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmsChange_Click(object sender, EventArgs e)
        {
            if(dgvCarsInformation.SelectedRows.Count == 1)
            {
                int index = dgvCarsInformation.SelectedRows[0].Index;
                CarParams car = new CarParams();
                car.Id = Int32.Parse(dgvCarsInformation.Rows[index].Cells[0].Value.ToString());
                car.Number = dgvCarsInformation.Rows[index].Cells[1].Value.ToString();
                car.EntryDate = DateTime.Parse(dgvCarsInformation.Rows[index].Cells[2].Value.ToString());
                if (dgvCarsInformation.Rows[index].Cells[3].Value != null)
                {
                    car.DepartureDate = DateTime.Parse(dgvCarsInformation.Rows[index].Cells[3].Value.ToString());
                }
                //Передаем имеющуюся информацию по клиенту
                FormChangeParams fChange = new FormChangeParams(car, mdb);
                fChange.ShowDialog();
                //Получить свежие данные по клиентам из бд
                carList = mdb.GetAllCars();
                DoSetAllCarList();
            }
            
        }
        /// <summary>
        /// Открыть форму для редактирования данных двойным нажатием
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvCarsInformation_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvCarsInformation.SelectedRows.Count == 1)
            {
                int index = dgvCarsInformation.SelectedRows[0].Index;
                CarParams car = new CarParams();
                car.Id = Int32.Parse(dgvCarsInformation.Rows[index].Cells[0].Value.ToString());
                car.Number = dgvCarsInformation.Rows[index].Cells[1].Value.ToString();
                car.EntryDate = DateTime.Parse(dgvCarsInformation.Rows[index].Cells[2].Value.ToString());
                if (dgvCarsInformation.Rows[index].Cells[3].Value != null)
                {
                    car.DepartureDate = DateTime.Parse(dgvCarsInformation.Rows[index].Cells[3].Value.ToString());
                }
                FormChangeParams fChange = new FormChangeParams(car, mdb);
                fChange.ShowDialog();
                carList = mdb.GetAllCars();
                DoSetAllCarList();
            }
        }
    }
}
