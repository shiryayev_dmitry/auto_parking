using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoparking
{
    public partial class Main : Form
    {
        /// <summary>
        /// ������������ ������� �����
        /// </summary>
        Timer timer = new Timer();
        /// <summary>
        /// ����� � ��
        /// </summary>
        mdb mdb = new mdb();
        public Main()
        {
            InitializeComponent();
            //����������� � ��
            mdb.DoConnected("../../../Autoparking.mdb");
            //������������ ������������� ��������� �������
            timer.Tick += new EventHandler(RefreshLabel);
            //��������� ���������
            timer.Interval = 1000;
            timer.Start();
            //���������� �������� �� ��������
            DoSetCarOnParking();
        }
        /// <summary>
        /// ������������ �� lb �������� �� ��������
        /// </summary>
        public void DoSetCarOnParking()
        {
            //������ � ���������
            List<CarParams> carList = mdb.GetCarsOnParking();
            lbCars.Items.Clear();
            foreach(CarParams cars in carList)
            {
                //������������ ������
                string carInfo = String.Format("{0}|{1}", cars.Number.PadRight(10), cars.EntryDate.ToString("G").PadLeft(25));
                lbCars.Items.Add(carInfo);
            }
        }
        //����������� ��������� �����
        public void RefreshLabel(object sender, EventArgs e)
        {
            LTime.Text = DateTime.Now.ToString("G");
        }
        //�������� ������ ��������
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DoSetCarOnParking();
        }
        /// <summary>
        /// ������� ����� �������� ������ �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEntance_Click(object sender, EventArgs e)
        {
            FormCreateNewCar fNewCar = new FormCreateNewCar(mdb);
            fNewCar.ShowDialog();
            //�������� ������ �������� ����� �������� �����
            DoSetCarOnParking();
        }
        /// <summary>
        /// ���������������� ����� � ��������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOutside_Click(object sender, EventArgs e)
        {
            if (lbCars.SelectedItem != null)
            {
                string departureCar = lbCars.SelectedItem.ToString();
                //���������������� � ��
                mdb.DoUpdateDepartureDateByNumber(departureCar.Split('|')[0].Trim(), DateTime.Now);
                DoSetCarOnParking();
            }
            else
            {
                MessageBox.Show("�� �� ������� ������");
            }
        }
        /// <summary>
        /// ������� ����� ���������� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnControl_Click(object sender, EventArgs e)
        {
            FormTerminalControl fControl = new FormTerminalControl(mdb);
            fControl.ShowDialog();
            DoSetCarOnParking();
        }
    }
}
