﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoparking
{
    public partial class FormCreateNewCar : Form
    {
        mdb mdb;
        public FormCreateNewCar(mdb _mdb)
        {
            InitializeComponent();
            //Получить уже существующий класс с установленным соединением с бд
            mdb = _mdb;
            //Выставить текующую дату
            dtpEntryDate.Value = DateTime.Now;
            //Выставить текущее время
            dtpEntryTime.Value = DateTime.Now;
        }
        //Зарегистрировать в бд нового клиента
        private void btnRegustry_Click(object sender, EventArgs e)
        {
            CarParams car = new CarParams();
            car.Number = tbNumber.Text.ToUpper();
            //Сформировать полную дату и время въезда машины на парковку
            car.EntryDate = DateTime.Parse(String.Format("{0} {1}", dtpEntryDate.Value.ToShortDateString(), dtpEntryTime.Value.ToShortTimeString()));
            mdb.DoRegistrу(car);
            this.Close();
        }
    }
}
