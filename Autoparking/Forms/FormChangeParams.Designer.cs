﻿namespace Autoparking
{
    partial class FormChangeParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNumber = new System.Windows.Forms.Label();
            this.lbEntryDate = new System.Windows.Forms.Label();
            this.lbDepartureDate = new System.Windows.Forms.Label();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpEntryDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEntryTime = new System.Windows.Forms.DateTimePicker();
            this.dtpDepartureTime = new System.Windows.Forms.DateTimePicker();
            this.dtpDepartureDate = new System.Windows.Forms.DateTimePicker();
            this.btnChangeDepartureDate = new System.Windows.Forms.Button();
            this.btnDeleteDepartureDate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbNumber
            // 
            this.lbNumber.AutoSize = true;
            this.lbNumber.Location = new System.Drawing.Point(17, 16);
            this.lbNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNumber.Name = "lbNumber";
            this.lbNumber.Size = new System.Drawing.Size(134, 17);
            this.lbNumber.TabIndex = 0;
            this.lbNumber.Text = "Номер автомобиля";
            // 
            // lbEntryDate
            // 
            this.lbEntryDate.AutoSize = true;
            this.lbEntryDate.Location = new System.Drawing.Point(17, 71);
            this.lbEntryDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbEntryDate.Name = "lbEntryDate";
            this.lbEntryDate.Size = new System.Drawing.Size(93, 17);
            this.lbEntryDate.TabIndex = 1;
            this.lbEntryDate.Text = "Дата въезда";
            // 
            // lbDepartureDate
            // 
            this.lbDepartureDate.AutoSize = true;
            this.lbDepartureDate.Location = new System.Drawing.Point(16, 135);
            this.lbDepartureDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDepartureDate.Name = "lbDepartureDate";
            this.lbDepartureDate.Size = new System.Drawing.Size(94, 17);
            this.lbDepartureDate.TabIndex = 2;
            this.lbDepartureDate.Text = "Дата выезда";
            // 
            // tbNumber
            // 
            this.tbNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNumber.Location = new System.Drawing.Point(177, 16);
            this.tbNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(389, 22);
            this.tbNumber.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(392, 176);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(175, 28);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Сохранить изменения";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpEntryDate
            // 
            this.dtpEntryDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEntryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEntryDate.Location = new System.Drawing.Point(177, 71);
            this.dtpEntryDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpEntryDate.Name = "dtpEntryDate";
            this.dtpEntryDate.Size = new System.Drawing.Size(97, 22);
            this.dtpEntryDate.TabIndex = 7;
            // 
            // dtpEntryTime
            // 
            this.dtpEntryTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEntryTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEntryTime.Location = new System.Drawing.Point(284, 71);
            this.dtpEntryTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpEntryTime.Name = "dtpEntryTime";
            this.dtpEntryTime.Size = new System.Drawing.Size(97, 22);
            this.dtpEntryTime.TabIndex = 8;
            // 
            // dtpDepartureTime
            // 
            this.dtpDepartureTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDepartureTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpDepartureTime.Location = new System.Drawing.Point(284, 135);
            this.dtpDepartureTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpDepartureTime.Name = "dtpDepartureTime";
            this.dtpDepartureTime.Size = new System.Drawing.Size(97, 22);
            this.dtpDepartureTime.TabIndex = 10;
            this.dtpDepartureTime.Visible = false;
            // 
            // dtpDepartureDate
            // 
            this.dtpDepartureDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDepartureDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDepartureDate.Location = new System.Drawing.Point(177, 135);
            this.dtpDepartureDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpDepartureDate.Name = "dtpDepartureDate";
            this.dtpDepartureDate.Size = new System.Drawing.Size(97, 22);
            this.dtpDepartureDate.TabIndex = 9;
            this.dtpDepartureDate.Visible = false;
            // 
            // btnChangeDepartureDate
            // 
            this.btnChangeDepartureDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeDepartureDate.Location = new System.Drawing.Point(177, 132);
            this.btnChangeDepartureDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnChangeDepartureDate.Name = "btnChangeDepartureDate";
            this.btnChangeDepartureDate.Size = new System.Drawing.Size(205, 28);
            this.btnChangeDepartureDate.TabIndex = 11;
            this.btnChangeDepartureDate.Text = "Ввести дату выезда";
            this.btnChangeDepartureDate.UseVisualStyleBackColor = true;
            this.btnChangeDepartureDate.Click += new System.EventHandler(this.btnChangeDepartureDate_Click);
            // 
            // btnDeleteDepartureDate
            // 
            this.btnDeleteDepartureDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteDepartureDate.Location = new System.Drawing.Point(392, 132);
            this.btnDeleteDepartureDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteDepartureDate.Name = "btnDeleteDepartureDate";
            this.btnDeleteDepartureDate.Size = new System.Drawing.Size(176, 28);
            this.btnDeleteDepartureDate.TabIndex = 12;
            this.btnDeleteDepartureDate.Text = "Удалить дату выезда";
            this.btnDeleteDepartureDate.UseVisualStyleBackColor = true;
            this.btnDeleteDepartureDate.Visible = false;
            this.btnDeleteDepartureDate.Click += new System.EventHandler(this.btnDeleteDepartureDate_Click);
            // 
            // FormChangeParams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 219);
            this.Controls.Add(this.btnDeleteDepartureDate);
            this.Controls.Add(this.btnChangeDepartureDate);
            this.Controls.Add(this.dtpDepartureTime);
            this.Controls.Add(this.dtpDepartureDate);
            this.Controls.Add(this.dtpEntryTime);
            this.Controls.Add(this.dtpEntryDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbNumber);
            this.Controls.Add(this.lbDepartureDate);
            this.Controls.Add(this.lbEntryDate);
            this.Controls.Add(this.lbNumber);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(599, 256);
            this.Name = "FormChangeParams";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Изменить данные";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNumber;
        private System.Windows.Forms.Label lbEntryDate;
        private System.Windows.Forms.Label lbDepartureDate;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpEntryDate;
        private System.Windows.Forms.DateTimePicker dtpEntryTime;
        private System.Windows.Forms.DateTimePicker dtpDepartureTime;
        private System.Windows.Forms.DateTimePicker dtpDepartureDate;
        private System.Windows.Forms.Button btnChangeDepartureDate;
        private System.Windows.Forms.Button btnDeleteDepartureDate;
    }
}