﻿namespace Autoparking
{
    partial class FormTerminalControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvCarsInformation = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSetDayClient = new System.Windows.Forms.Button();
            this.btnSetAllClient = new System.Windows.Forms.Button();
            this.cmsDeparture = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsChange = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsCarProps = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarsInformation)).BeginInit();
            this.cmsCarProps.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCarsInformation
            // 
            this.dgvCarsInformation.AllowUserToAddRows = false;
            this.dgvCarsInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCarsInformation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCarsInformation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCarsInformation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Number,
            this.EntryDate,
            this.DepartureDate});
            this.dgvCarsInformation.Location = new System.Drawing.Point(17, 52);
            this.dgvCarsInformation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvCarsInformation.Name = "dgvCarsInformation";
            this.dgvCarsInformation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvCarsInformation.RowHeadersVisible = false;
            this.dgvCarsInformation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCarsInformation.Size = new System.Drawing.Size(613, 290);
            this.dgvCarsInformation.TabIndex = 0;
            this.dgvCarsInformation.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCarsInformation_CellMouseDoubleClick);
            this.dgvCarsInformation.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCarsInformation_CellMouseDown);
            // 
            // id
            // 
            this.id.HeaderText = "Id";
            this.id.Name = "id";
            // 
            // Number
            // 
            this.Number.HeaderText = "Номер";
            this.Number.Name = "Number";
            // 
            // EntryDate
            // 
            this.EntryDate.HeaderText = "Дата въезда";
            this.EntryDate.Name = "EntryDate";
            // 
            // DepartureDate
            // 
            this.DepartureDate.HeaderText = "Дата выезда";
            this.DepartureDate.Name = "DepartureDate";
            // 
            // btnSetDayClient
            // 
            this.btnSetDayClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetDayClient.Location = new System.Drawing.Point(397, 16);
            this.btnSetDayClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSetDayClient.Name = "btnSetDayClient";
            this.btnSetDayClient.Size = new System.Drawing.Size(232, 28);
            this.btnSetDayClient.TabIndex = 1;
            this.btnSetDayClient.Text = "Клиенты за последние 24 часа";
            this.btnSetDayClient.UseVisualStyleBackColor = true;
            this.btnSetDayClient.Click += new System.EventHandler(this.btnSetDayClient_Click);
            // 
            // btnSetAllClient
            // 
            this.btnSetAllClient.Location = new System.Drawing.Point(17, 16);
            this.btnSetAllClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSetAllClient.Name = "btnSetAllClient";
            this.btnSetAllClient.Size = new System.Drawing.Size(205, 28);
            this.btnSetAllClient.TabIndex = 2;
            this.btnSetAllClient.Text = "Клиенты за всё время";
            this.btnSetAllClient.UseVisualStyleBackColor = true;
            this.btnSetAllClient.Click += new System.EventHandler(this.btnSetAllClient_Click);
            // 
            // cmsDeparture
            // 
            this.cmsDeparture.Name = "cmsDeparture";
            this.cmsDeparture.Size = new System.Drawing.Size(251, 24);
            this.cmsDeparture.Text = "Зарегистрировать выезд";
            this.cmsDeparture.Click += new System.EventHandler(this.cmsDeparture_Click);
            // 
            // cmsChange
            // 
            this.cmsChange.Name = "cmsChange";
            this.cmsChange.Size = new System.Drawing.Size(251, 24);
            this.cmsChange.Text = "Исправить данные";
            this.cmsChange.Click += new System.EventHandler(this.cmsChange_Click);
            // 
            // cmsCarProps
            // 
            this.cmsCarProps.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsCarProps.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsDeparture,
            this.cmsChange});
            this.cmsCarProps.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.cmsCarProps.Name = "cmsCarProps";
            this.cmsCarProps.Size = new System.Drawing.Size(252, 52);
            // 
            // FormTerminalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 357);
            this.Controls.Add(this.btnSetAllClient);
            this.Controls.Add(this.btnSetDayClient);
            this.Controls.Add(this.dgvCarsInformation);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(630, 373);
            this.Name = "FormTerminalControl";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление терминалом";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarsInformation)).EndInit();
            this.cmsCarProps.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSetDayClient;
        private System.Windows.Forms.Button btnSetAllClient;
        private System.Windows.Forms.DataGridView dgvCarsInformation;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureDate;
        private System.Windows.Forms.ToolStripMenuItem cmsDeparture;
        private System.Windows.Forms.ToolStripMenuItem cmsChange;
        private System.Windows.Forms.ContextMenuStrip cmsCarProps;
    }
}