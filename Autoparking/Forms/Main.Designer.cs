namespace Autoparking
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCars = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEntance = new System.Windows.Forms.Button();
            this.LTime = new System.Windows.Forms.Label();
            this.btnOutside = new System.Windows.Forms.Button();
            this.btnControl = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbCars
            // 
            this.lbCars.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCars.FormattingEnabled = true;
            this.lbCars.Location = new System.Drawing.Point(13, 44);
            this.lbCars.Name = "lbCars";
            this.lbCars.Size = new System.Drawing.Size(230, 212);
            this.lbCars.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "������� �� �������:";
            // 
            // btnEntance
            // 
            this.btnEntance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEntance.Location = new System.Drawing.Point(279, 44);
            this.btnEntance.Name = "btnEntance";
            this.btnEntance.Size = new System.Drawing.Size(170, 23);
            this.btnEntance.TabIndex = 2;
            this.btnEntance.Text = "����� �� ��������";
            this.btnEntance.UseVisualStyleBackColor = true;
            this.btnEntance.Click += new System.EventHandler(this.btnEntance_Click);
            // 
            // LTime
            // 
            this.LTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LTime.AutoSize = true;
            this.LTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LTime.Location = new System.Drawing.Point(276, 240);
            this.LTime.Name = "LTime";
            this.LTime.Size = new System.Drawing.Size(0, 16);
            this.LTime.TabIndex = 5;
            // 
            // btnOutside
            // 
            this.btnOutside.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOutside.Location = new System.Drawing.Point(279, 73);
            this.btnOutside.Name = "btnOutside";
            this.btnOutside.Size = new System.Drawing.Size(170, 23);
            this.btnOutside.TabIndex = 6;
            this.btnOutside.Text = "����� � ��������";
            this.btnOutside.UseVisualStyleBackColor = true;
            this.btnOutside.Click += new System.EventHandler(this.btnOutside_Click);
            // 
            // btnControl
            // 
            this.btnControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnControl.Location = new System.Drawing.Point(279, 102);
            this.btnControl.Name = "btnControl";
            this.btnControl.Size = new System.Drawing.Size(170, 23);
            this.btnControl.TabIndex = 7;
            this.btnControl.Text = "���������� ����������";
            this.btnControl.UseVisualStyleBackColor = true;
            this.btnControl.Click += new System.EventHandler(this.btnControl_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(279, 131);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(170, 23);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "�������� ������ ��������";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 273);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnControl);
            this.Controls.Add(this.btnOutside);
            this.Controls.Add(this.LTime);
            this.Controls.Add(this.btnEntance);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCars);
            this.MinimumSize = new System.Drawing.Size(477, 312);
            this.Name = "Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Autoparking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbCars;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEntance;
        private System.Windows.Forms.Label LTime;
        private System.Windows.Forms.Button btnOutside;
        private System.Windows.Forms.Button btnControl;
        private System.Windows.Forms.Button btnUpdate;
    }
}

