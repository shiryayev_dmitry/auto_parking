﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoparking
{
    public class mdb
    {
        public OleDbConnection g_connection;
        /// <summary>
        /// Установить соединение
        /// </summary>
        /// <param name="path"></param>
        public void DoConnected(string path)
        {
            try
            {
                g_connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" +
                                 path + "\"; Jet OLEDB:Database Password=\"\";" +
                                 "Jet OLEDB:Engine Type=5;Jet OLEDB:Encrypt Database=True;");
                g_connection.Open();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Произошла ошибка в момент установления соединения с бд: " + ex.Message);
            }
        }
        /// <summary>
        /// все имеющиеся клиенты
        /// </summary>
        /// <returns></returns>
        public List<CarParams> GetAllCars()
        {
            List<CarParams> carList = new List<CarParams>();
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT car_id, car_number, entry_date, departure_date FROM Autoparking";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CarParams carParams = new CarParams();
                        carParams.Id = reader.GetInt32(0);
                        carParams.Number = reader.GetString(1);
                        carParams.EntryDate = reader.GetDateTime(2);
                        if (!reader.IsDBNull(3))
                        {
                            carParams.DepartureDate = reader.GetDateTime(3);
                        }
                        carList.Add(carParams);
                    }
                }
            }
            return carList;
        }
        /// <summary>
        /// все клиенты на парковке
        /// </summary>
        /// <returns></returns>
        public List<CarParams> GetCarsOnParking()
        {
            List<CarParams> carList = new List<CarParams>();
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT car_id, car_number, entry_date FROM Autoparking WHERE departure_date IS NULL";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        CarParams carParams = new CarParams();
                        carParams.Id = reader.GetInt32(0);
                        carParams.Number = reader.GetString(1);
                        carParams.EntryDate = reader.GetDateTime(2);
                        carList.Add(carParams);
                    }
                }
            }
            return carList;
        }
        /// <summary>
        /// зарегистрировать въезд
        /// </summary>
        /// <param name="carParams"></param>
        public void DoRegistrу(CarParams carParams)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO Autoparking (car_number, entry_date)
                                    VALUES (?,?)";
                cmd.Parameters.AddWithValue("car_number", carParams.Number);
                cmd.Parameters.AddWithValue("entry_date", carParams.EntryDate);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// обновить информацию о клиенте по номеру
        /// </summary>
        /// <param name="number"></param>
        /// <param name="date"></param>
        public void DoUpdateDepartureDateByNumber(string number, DateTime date)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"UPDATE Autoparking SET departure_date=@departure_date WHERE car_number=@car_number";
                cmd.Parameters.AddWithValue("departure_date", date.ToString("G"));
                cmd.Parameters.AddWithValue("car_number", number);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// изменить данные о клиенте по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="carParams"></param>
        public void DoChangeDataById(int id, CarParams carParams)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE Autoparking SET car_number=@car_number, entry_date=@entry_date, departure_date=@departure_date WHERE car_id=@car_id";
                cmd.Parameters.AddWithValue("car_number", carParams.Number);
                cmd.Parameters.AddWithValue("entry_date", carParams.EntryDate);
                if (carParams.DepartureDate.ToString("G") != new DateTime().ToString("G"))
                {
                    cmd.Parameters.AddWithValue("departure_date", carParams.DepartureDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("departure_date", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("car_id", id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
